package com.example.samplemapbox;

import androidx.core.app.ActivityCompat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.samplemapbox.activity.HomeActivity;
import com.example.samplemapbox.services.LocationPickService;
import com.example.samplemapbox.utils.AnimatorUtils;
import com.example.samplemapbox.utils.Vars;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;
import java.util.List;

import static com.example.samplemapbox.utils.Support.GetPrefDefault;

public class IndexActivity extends BaseActivity {
    public final static int PERMISSION_LOCATION = 2711;
    public final static int REQUEST_CHECK_SETTINGS = 2710;
    LocationManager locationManager;
    Handler handlerLatLng = new Handler();
    Runnable runnableLatLng;
    boolean flag = false;
    int Repeat = 0;
    TextView txtVersion;
    ImageView imgLogo;
    ImageView imgCompanyLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        imgLogo = findViewById(R.id.imgLogo);
        imgCompanyLogo = findViewById(R.id.imgCompanyLogo);
        txtVersion = findViewById(R.id.txtVersion);
        showPinDrop();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!IsLocationPermissionGranted()) {
            showLocationPermissionAlert();
        } else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            displayLocationSettingsRequest(this);
        } else {
            startBackgroundService();
        }

    }

    private void showPinDrop() {
        List<Animator> animList = new ArrayList<>();
        animList.add(createPinShowItemAnimator(imgLogo));
        AnimatorSet animSet = new AnimatorSet();
        animSet.setDuration(1500);
        animSet.setInterpolator(new BounceInterpolator());
        animSet.playTogether(animList);
        animSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                imgCompanyLogo.setVisibility(View.VISIBLE);
            }
        });
        animSet.start();
    }

    private Animator createPinShowItemAnimator(View item) {

        item.setTranslationX(0f);
        item.setTranslationY(-600f);

        Animator anim = ObjectAnimator.ofPropertyValuesHolder(
                item,
                AnimatorUtils.translationX(0f),
                AnimatorUtils.translationY(0f)
        );

        return anim;
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        if (requestCode == PERMISSION_LOCATION) {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                displayLocationSettingsRequest(this);
            } else {
                startBackgroundService();
            }
        }
    }

    public void startBackgroundService() {
        if (!flag) {
            flag = true;
            startService(new Intent(IndexActivity.this, LocationPickService.class));
            runnableLatLng = new Runnable() {
                @Override
                public void run() {
                    String Current_Latlng = GetPrefDefault(IndexActivity.this, Vars.Pref_Current_Latitude, "0.0") + "," + GetPrefDefault(IndexActivity.this, Vars.Pref_Current_Longitude, "0.0");
                    if ((!Current_Latlng.equals("0.0,0.0") && !Current_Latlng.equals("")) || Repeat > 10) {
                        startActivity(new Intent(IndexActivity.this, HomeActivity.class));
                        //   new doGetAddress(new LatLng(ConvertToDouble(GetPref(IndexActivity.this, Vars.Pref_Current_Latitude)), ConvertToDouble(GetPref(IndexActivity.this, Vars.Pref_Current_Longitude)))).execute();
                    } else {
                        Repeat++;
                        handlerLatLng.postDelayed(runnableLatLng, 1000);
                    }
                }
            };
            handlerLatLng.postDelayed(runnableLatLng, 1000);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode) {
        if (requestCode == PERMISSION_LOCATION) {
            showLocationPermissionAlert();
        }
    }

    public void showLocationPermissionAlert() {
        IndexActivity.super.requestAppPermissions(new
                String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION);
    }

    private boolean IsLocationPermissionGranted() {
        return !(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);
    }

    private void displayLocationSettingsRequest(final Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startBackgroundService();
                        Log.e("Map", "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.e("Map", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(IndexActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.e("Map", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e("Map", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

}
