package com.example.samplemapbox.services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.samplemapbox.R;
import com.example.samplemapbox.utils.Support;
import com.example.samplemapbox.utils.Vars;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.location.LocationEngineResult;

import java.lang.ref.WeakReference;

public class LocationPickService extends Service {

    private MainActivityLocationCallback callback = new MainActivityLocationCallback();
    private LocationEngine locationEngine;
    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;

    public LocationPickService() {
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        initLocationEngine();
        return Service.START_STICKY;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this);

        LocationEngineRequest request = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
               return;
        }
        locationEngine.requestLocationUpdates(request, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    protected void LocationChanged(Location location) {
        Support.SetPref(getApplicationContext(), Vars.Pref_Current_Latitude, location.getLatitude() + "");
        Support.SetPref(getApplicationContext(), Vars.Pref_Current_Longitude, location.getLongitude() + "");
        Toast.makeText(getApplicationContext(), "Location Updated", Toast.LENGTH_LONG).show();
    }

    private class MainActivityLocationCallback implements LocationEngineCallback<LocationEngineResult> {

        MainActivityLocationCallback() {
        }

        @Override
        public void onSuccess(LocationEngineResult result) {

            Location location = result.getLastLocation();

            if (location == null) {
                return;
            }

            LocationChanged(result.getLastLocation());
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
        }
    }

}
