package com.example.samplemapbox.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.samplemapbox.BaseActivity;
import com.example.samplemapbox.R;
import com.example.samplemapbox.fragment.HomeFragment;
import com.google.android.material.navigation.NavigationView;
import com.mapbox.mapboxsdk.Mapbox;

import retrofit2.Response;

public class HomeActivity extends BaseActivity implements View.OnClickListener {

    Fragment fragment = null;
    FragmentTransaction ft;
    DrawerLayout drawer;
    LinearLayout  linearBookRide, linearRideARent;
    ImageView imgNavImage;
    TextView txtNavName, txtNavPhone, txtNavEmail, txtNavAccessKey;
    TextView txtVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, "pk.eyJ1Ijoic2RsaW5nYW03IiwiYSI6ImNrMDIxaGR3eDBmZjYzY3FvZmlhNTB1MHkifQ.T-rBBI3eyeJaOP6uQe7xjw");
        setContentView(R.layout.activity_home);

        activity = HomeActivity.this;

        drawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        UpdateNavBar(navigationView);

        MoveToMapScreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        activity = HomeActivity.this;
    }

    public void removeMenuBack() {
        linearBookRide.setSelected(false);
        linearRideARent.setSelected(false);
    }

    @Override
    public void onClick(View v) {
        removeMenuBack();
        switch (v.getId()) {
            case R.id.linearBookRide:
                linearBookRide.setSelected(true);
                fragment = new HomeFragment();
                break;
            case R.id.linearRideARent:
                linearRideARent.setSelected(true);
                fragment = new HomeFragment();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        if (fragment != null) {
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frameContent, fragment);
            ft.commit();
        }

    }

    public void MoveToMapScreen() {
        removeMenuBack();
        fragment = new HomeFragment();
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameContent, fragment);
        ft.commit();
        linearBookRide.setSelected(true);
    }

    public void UpdateNavBar(NavigationView navigationView) {

        linearBookRide = navigationView.findViewById(R.id.linearBookRide);
        linearRideARent = navigationView.findViewById(R.id.linearRideARent);

        linearBookRide.setOnClickListener(this);
        linearRideARent.setOnClickListener(this);

    }

    @Override
    public void onPermissionsGranted(int requestCode) {

    }

    public void dummy(View view) {
    }

    public void onClickMenu() {
        drawer.openDrawer(GravityCompat.START);
    }
}
