package com.example.samplemapbox;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samplemapbox.utils.Support;
import com.example.samplemapbox.utils.Vars;
import com.google.android.gms.maps.model.LatLng;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.MapboxDirections;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerView;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerViewManager;
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker;
import com.mapbox.mapboxsdk.plugins.places.picker.model.PlacePickerOptions;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.widget.ListPopupWindow.WRAP_CONTENT;
import static com.example.samplemapbox.utils.ConvertUtils.ConvertToDouble;
import static com.example.samplemapbox.utils.Support.GetPref;
import static com.example.samplemapbox.utils.Support.formatLatLngFromString;
import static com.mapbox.core.constants.Constants.PRECISION_6;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;

public class MainActivity extends BaseActivity implements OnMapReadyCallback, PermissionsListener, View.OnClickListener {
    private MapView mapView;
    String selectionLocation = "";
    private MapboxMap mapboxMap;
    private PermissionsManager permissionsManager;
    LatLng prevSourceLatLng = null, sourceLatLng = null, destinationLatLng = null;
    Location sourceLocation;
    CardView cardSource, cardDrop;
    TextView txtPickupLocation, txtDropLocation;
    MarkerViewManager markerViewManager;
    MarkerView sourceMarker, dropMarker;
    private FeatureCollection dashedLineDirectionsFeatureCollection;


    private static final int REQUEST_CODE = 5678;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, "pk.eyJ1Ijoic2RsaW5nYW03IiwiYSI6ImNrMDIxaGR3eDBmZjYzY3FvZmlhNTB1MHkifQ.T-rBBI3eyeJaOP6uQe7xjw");
        setContentView(R.layout.activity_main);
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        cardSource = findViewById(R.id.cardSource);
        cardDrop = findViewById(R.id.cardDrop);

        txtPickupLocation = findViewById(R.id.txtPickupLocation);
        txtDropLocation = findViewById(R.id.txtDropLocation);

        loadCurrentLocationForSource();

        cardSource.setOnClickListener(this);
        cardDrop.setOnClickListener(this);
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;

        mapboxMap.setStyle(Style.TRAFFIC_DAY,
                new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);
                        initDottedLineSourceAndLayer(style);
                        markerViewManager = new MarkerViewManager(mapView, mapboxMap);
                        setupMarkerNavigation();

                    }
                });
    }

    private void initDottedLineSourceAndLayer(@NonNull Style loadedMapStyle) {
        dashedLineDirectionsFeatureCollection = FeatureCollection.fromFeatures(new Feature[] {});
        loadedMapStyle.addSource(new GeoJsonSource("SOURCE_ID", dashedLineDirectionsFeatureCollection));
        loadedMapStyle.addLayerBelow(
                new LineLayer(
                        "DIRECTIONS_LAYER_ID", "SOURCE_ID").withProperties(
                        lineWidth(4.0f),
                        lineColor(Color.RED)
                ), "road-label-small");
    }

    private void loadCurrentLocationForSource() {
        sourceLatLng = formatLatLngFromString(GetPref(this, Vars.Pref_Current_Latitude) + "," + GetPref(this, Vars.Pref_Current_Longitude));

        new doGetAddress(sourceLatLng, "source").execute();
        sourceLocation = new Location(LocationManager.GPS_PROVIDER);
        sourceLocation.setLatitude(ConvertToDouble(GetPref(this, Vars.Pref_Current_Latitude)));
        sourceLocation.setLongitude(ConvertToDouble(GetPref(this, Vars.Pref_Current_Longitude)));
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();

            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);

            mapboxMap.getLocationComponent().forceLocationUpdate(sourceLocation);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        markerViewManager.onDestroy();
        mapView.onDestroy();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }


    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
            }
        } else {
            Toast.makeText(this, "Not Permitted", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cardSource:
                selectionLocation = "source";
                goToPickerActivity();
                break;
            case R.id.cardDrop:
                selectionLocation = "drop";
                goToPickerActivity();
                break;
        }
    }

    private void goToPickerActivity() {
        double latitude = ConvertToDouble(GetPref(this, Vars.Pref_Current_Latitude));
        double longitude = ConvertToDouble(GetPref(this, Vars.Pref_Current_Longitude));
        startActivityForResult(
                new PlacePicker.IntentBuilder()
                        .accessToken(getString(R.string.access_token))
                        .placeOptions(PlacePickerOptions.builder()
                                .statingCameraPosition(new CameraPosition.Builder()
                                        .target(new com.mapbox.mapboxsdk.geometry.LatLng(latitude, longitude)).zoom(16).build())
                                .build())
                        .build(this), REQUEST_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            Button goToPickerActivityButton = findViewById(R.id.go_to_picker_button);
            goToPickerActivityButton.setVisibility(View.VISIBLE);
            goToPickerActivityButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goToPickerActivity();
                }
            });
        } else if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            CarmenFeature carmenFeature = PlacePicker.getPlace(data);
            try {
                JSONObject jsonObject = new JSONObject(carmenFeature.toJson());
                String address = jsonObject.getString("place_name");

                JSONObject geometry = jsonObject.getJSONObject("geometry");
                String latitude = geometry.getJSONArray("coordinates").getString(1);
                String longitude = geometry.getJSONArray("coordinates").getString(0);

                if (selectionLocation.equals("source")) {
                    sourceLatLng = formatLatLngFromString(latitude + "," + longitude);
                    txtPickupLocation.setText(address);
                } else {
                    destinationLatLng = formatLatLngFromString(latitude + "," + longitude);
                    txtDropLocation.setText(address);
                }
                setupMarkerNavigation();
            } catch (Exception ignored) {

            }
        }
    }

    public void setupMarkerNavigation() {
        if (sourceMarker != null) {
            markerViewManager.removeMarker(sourceMarker);
        }
        if (dropMarker != null) {
            markerViewManager.removeMarker(dropMarker);
        }
        if (sourceLatLng != null) {
            View customView = LayoutInflater.from(this).inflate(
                    R.layout.view_source, null);
            customView.setLayoutParams(new FrameLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT));
            com.mapbox.mapboxsdk.geometry.LatLng latLng = new com.mapbox.mapboxsdk.geometry.LatLng(sourceLatLng.latitude, sourceLatLng.longitude);
            sourceMarker = new MarkerView(latLng, customView);
            markerViewManager.addMarker(sourceMarker);
        }
        if (destinationLatLng != null) {

            View customView = LayoutInflater.from(this).inflate(
                    R.layout.view_source, null);
            customView.setLayoutParams(new FrameLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT));
            com.mapbox.mapboxsdk.geometry.LatLng latLng = new com.mapbox.mapboxsdk.geometry.LatLng(destinationLatLng.latitude, destinationLatLng.longitude);
            dropMarker = new MarkerView(latLng, customView);
            markerViewManager.addMarker(dropMarker);
        }
        if (sourceLatLng != null && destinationLatLng != null) {
            Point sourcePoint = Point.fromLngLat(
                    sourceLatLng.longitude, sourceLatLng.latitude);
            Point dropPoint = Point.fromLngLat(
                    destinationLatLng.longitude, destinationLatLng.latitude);
            getRoute(sourcePoint, dropPoint);
        }
    }


    @SuppressWarnings({"MissingPermission"})
    private void getRoute(Point source, Point destination) {
        MapboxDirections client = MapboxDirections.builder()
                .origin(source)
                .destination(destination)
                .overview(DirectionsCriteria.OVERVIEW_FULL)
                .profile(DirectionsCriteria.PROFILE_DRIVING)
                .accessToken(getString(R.string.access_token))
                .build();
        client.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                if (response.body() == null) {
                    Timber.d("No routes found, make sure you set the right user and access token.");
                    return;
                } else if (response.body().routes().size() < 1) {
                    Timber.d("No routes found");
                    return;
                }
                drawNavigationPolylineRoute(response.body().routes().get(0));
            }

            @Override
            public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                Timber.d("Error: %s", throwable.getMessage());
                if (!throwable.getMessage().equals("Coordinate is invalid: 0,0")) {
                    Toast.makeText(MainActivity.this,
                            "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void drawNavigationPolylineRoute(final DirectionsRoute route) {
        if (mapboxMap != null) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    List<Feature> directionsRouteFeatureList = new ArrayList<>();
                    LineString lineString = LineString.fromPolyline(route.geometry(), PRECISION_6);
                    List<Point> coordinates = lineString.coordinates();
                    for (int i = 0; i < coordinates.size(); i++) {
                        directionsRouteFeatureList.add(Feature.fromGeometry(LineString.fromLngLats(coordinates)));
                    }
                    dashedLineDirectionsFeatureCollection = FeatureCollection.fromFeatures(directionsRouteFeatureList);
                    GeoJsonSource source = style.getSourceAs("SOURCE_ID");
                    if (source != null) {
                        source.setGeoJson(dashedLineDirectionsFeatureCollection);
                    }
                }
            });
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class doGetAddress extends AsyncTask<Void, Void, String> {
        LatLng value;
        String purpose;

        doGetAddress(LatLng value, String purpose) {
            this.purpose = purpose;
            this.value = value;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            return getAddressByURL("https://api.mapbox.com/geocoding/v5/mapbox.places/" +
                    value.longitude + "," + value.latitude + ".json?access_token=" + getResources().getString(R.string.access_token));
        }

        @Override
        protected void onPostExecute(String ReadAddress) {
            super.onPostExecute(ReadAddress);
            try {
                JSONObject jsonObject = new JSONObject(ReadAddress);

                String formatted_address = ((JSONArray) jsonObject.get("features")).getJSONObject(0)
                        .getString("place_name");
                if (purpose.equals("source")) {
                    txtPickupLocation.setText(formatted_address);
                } else {
                    txtDropLocation.setText(formatted_address);
                }
            } catch (Exception e) {

            }
        }
    }

}