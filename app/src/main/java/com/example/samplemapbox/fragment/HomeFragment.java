package com.example.samplemapbox.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.example.samplemapbox.BaseFragment;
import com.example.samplemapbox.MainActivity;
import com.example.samplemapbox.R;
import com.example.samplemapbox.activity.HomeActivity;
import com.example.samplemapbox.utils.RetrofitObjectAPI;
import com.example.samplemapbox.utils.Vars;
import com.google.gson.Gson;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.MapboxDirections;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.CoordinateContainer;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerView;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerViewManager;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker;
import com.mapbox.mapboxsdk.plugins.places.picker.model.PlacePickerOptions;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.example.samplemapbox.utils.ConvertUtils.ConvertToDouble;
import static com.example.samplemapbox.utils.Support.GetPref;
import static com.mapbox.core.constants.Constants.PRECISION_6;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;

public class HomeFragment extends BaseFragment implements View.OnClickListener, OnMapReadyCallback, PermissionsListener {

    ImageView imgAppMenu;
    private MapView mapView;
    String selectionLocation = "";
    private MapboxMap mapboxMap;
    private PermissionsManager permissionsManager;
    LatLng prevSourceLatLng = null, sourceLatLng = null, destinationLatLng = null;
    Location sourceLocation;
    CardView cardSource, cardDrop;
    TextView txtPickupLocation, txtDropLocation;
    MarkerViewManager markerViewManager;
    MarkerOptions sourceMarker, dropMarker;
    private FeatureCollection dashedLineDirectionsFeatureCollection;
    private static final int REQUEST_CODE = 5678;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 5679;
    private String geojsonSourceLayerId = "geojsonSourceLayerId";


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home, container, false);

        imgAppMenu = view.findViewById(R.id.imgAppMenu);

        imgAppMenu.setOnClickListener(this);


        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);


        cardSource = view.findViewById(R.id.cardSource);
        cardDrop = view.findViewById(R.id.cardDrop);

        txtPickupLocation = view.findViewById(R.id.txtPickupLocation);
        txtDropLocation = view.findViewById(R.id.txtDropLocation);

        loadCurrentLocationForSource();

        cardSource.setOnClickListener(this);
        cardDrop.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgAppMenu:
                ((HomeActivity) activity).onClickMenu();
                break;
            case R.id.cardSource:
                selectionLocation = "source";
                goToPickerActivity(sourceLatLng);
                break;
            case R.id.cardDrop:
                selectionLocation = "drop";
                placeSelectionListActivity();
                break;
        }
    }

    private void placeSelectionListActivity() {
        Intent intent = new PlaceAutocomplete.IntentBuilder()
                .accessToken(getString(R.string.access_token))
                .placeOptions(PlaceOptions.builder()
                        .backgroundColor(Color.parseColor("#EEEEEE"))
                        .limit(10)
                        .build(PlaceOptions.MODE_CARDS))
                .build(activity);
        startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
    }


    private void goToPickerActivity(LatLng sourceLatLng) {
        startActivityForResult(
                new PlacePicker.IntentBuilder()
                        .accessToken(getString(R.string.access_token))
                        .placeOptions(PlacePickerOptions.builder()
                                .statingCameraPosition(new CameraPosition.Builder()
                                        .target(sourceLatLng).zoom(16).build())
                                .build())
                        .build(activity), REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

            CarmenFeature carmenFeature = PlacePicker.getPlace(data);
            try {

                String address =  carmenFeature.placeName();
                double latitude = carmenFeature.center().latitude();
                double longitude = carmenFeature.center().longitude();

                if (selectionLocation.equals("source")) {
                    sourceLatLng = new LatLng(latitude, longitude);
                    txtPickupLocation.setText(address);
                } else {
                    destinationLatLng = new LatLng(latitude, longitude);
                    txtDropLocation.setText(address);
                }
                setupMarkerNavigation();
            } catch (Exception ignored) {

            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            CarmenFeature carmenFeature = PlaceAutocomplete.getPlace(data);
            try {
                String address =  carmenFeature.placeName();
                double latitude = carmenFeature.center().latitude();
                double longitude = carmenFeature.center().longitude();

                if (selectionLocation.equals("source")) {
                    sourceLatLng = new LatLng(latitude, longitude);
                    txtPickupLocation.setText(address);
                } else {
                    destinationLatLng = new LatLng(latitude, longitude);
                    txtDropLocation.setText(address);
                }
                setupMarkerNavigation();
            } catch (Exception ignored) {

            }
        }
    }


    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;

        mapboxMap.setStyle(Style.MAPBOX_STREETS,
                new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);
                        initDottedLineSourceAndLayer(style);
                        markerViewManager = new MarkerViewManager(mapView, mapboxMap);
                        setupMarkerNavigation();
                    }
                });
    }

    private void initDottedLineSourceAndLayer(@NonNull Style loadedMapStyle) {
        dashedLineDirectionsFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        loadedMapStyle.addSource(new GeoJsonSource("SOURCE_ID", dashedLineDirectionsFeatureCollection));
        loadedMapStyle.addLayerBelow(
                new LineLayer(
                        "DIRECTIONS_LAYER_ID", "SOURCE_ID").withProperties(
                        lineWidth(3.0f),
                        lineColor(Color.RED)
                ), "road-label-small");
    }

    private void loadCurrentLocationForSource() {
        sourceLatLng = new LatLng();
        sourceLatLng.setLatitude(ConvertToDouble(GetPref(activity, Vars.Pref_Current_Latitude)));
        sourceLatLng.setLongitude(ConvertToDouble(GetPref(activity, Vars.Pref_Current_Longitude)));

        getAddress(sourceLatLng, "source");
        sourceLocation = new Location(LocationManager.GPS_PROVIDER);
        sourceLocation.setLatitude(ConvertToDouble(GetPref(activity, Vars.Pref_Current_Latitude)));
        sourceLocation.setLongitude(ConvertToDouble(GetPref(activity, Vars.Pref_Current_Longitude)));
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(activity)) {

            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(activity, loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();

            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);

            mapboxMap.getLocationComponent().forceLocationUpdate(sourceLocation);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(activity);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public void setupMarkerNavigation() {
        if (mapboxMap != null) {
            mapboxMap.clear();
        }

        if (sourceLatLng != null) {
            sourceMarker = new MarkerOptions()
                    .position(sourceLatLng).title("Source");

            mapboxMap.addMarker(sourceMarker);

        }
        if (destinationLatLng != null) {

            dropMarker = new MarkerOptions()
                    .position(destinationLatLng).title("Destination");

            mapboxMap.addMarker(dropMarker);
        }
        if (sourceLatLng != null && destinationLatLng != null) {
            Point sourcePoint = Point.fromLngLat(
                    sourceLatLng.getLongitude(), sourceLatLng.getLatitude());
            Point dropPoint = Point.fromLngLat(
                    destinationLatLng.getLongitude(), destinationLatLng.getLatitude());
            getRoute(sourcePoint, dropPoint);
        } else if (sourceLatLng != null) {
            CameraPosition position = new CameraPosition.Builder()
                    .target(sourceLatLng)
                    .zoom(16)
                    .build();
            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 150);
        }
    }


    @SuppressWarnings({"MissingPermission"})
    private void getRoute(Point source, Point destination) {

        LatLngBounds latLngBounds = new LatLngBounds.Builder()
                .include(sourceMarker.getPosition())
                .include(dropMarker.getPosition())
                .build();

        mapboxMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 10));


        MapboxDirections client = MapboxDirections.builder()
                .origin(source)
                .destination(destination)
                .overview(DirectionsCriteria.OVERVIEW_FULL)
                .profile(DirectionsCriteria.PROFILE_DRIVING)
                .accessToken(getString(R.string.access_token))
                .build();
        client.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                if (response.body() == null) {
                    Timber.d("No routes found, make sure you set the right user and access token.");
                    return;
                } else if (response.body().routes().size() < 1) {
                    Timber.d("No routes found");
                    return;
                }
                drawNavigationPolylineRoute(response.body().routes().get(0));
            }

            @Override
            public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                Timber.d("Error: %s", throwable.getMessage());
                if (!throwable.getMessage().equals("Coordinate is invalid: 0,0")) {
                    Toast.makeText(activity,
                            "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void drawNavigationPolylineRoute(final DirectionsRoute route) {
        if (mapboxMap != null) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    List<Feature> directionsRouteFeatureList = new ArrayList<>();
                    LineString lineString = LineString.fromPolyline(route.geometry(), PRECISION_6);
                    List<Point> coordinates = lineString.coordinates();
                    for (int i = 0; i < coordinates.size(); i++) {
                        directionsRouteFeatureList.add(Feature.fromGeometry(LineString.fromLngLats(coordinates)));
                    }
                    dashedLineDirectionsFeatureCollection = FeatureCollection.fromFeatures(directionsRouteFeatureList);
                    GeoJsonSource source = style.getSourceAs("SOURCE_ID");
                    if (source != null) {
                        source.setGeoJson(dashedLineDirectionsFeatureCollection);
                    }
                }
            });
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
            }
        } else {
            Toast.makeText(activity, "Not Permitted", Toast.LENGTH_LONG).show();
        }
    }

    public void getAddress(LatLng value, final String purpose) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.mapbox.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);

        Call<Object> call = service.getAddressByLatlng(value.getLongitude() + "," + value.getLatitude(), getResources().getString(R.string.access_token));


        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                try {
                    String stringJson = new Gson().toJson(response.body());
                    JSONObject jsonObject = new JSONObject(stringJson);

                    String formatted_address = ((JSONArray) jsonObject.get("features")).getJSONObject(0)
                            .getString("place_name");
                    if (purpose.equals("source")) {
                        txtPickupLocation.setText(formatted_address);
                    } else {
                        txtDropLocation.setText(formatted_address);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e("Error", t.getLocalizedMessage());
            }
        });
    }

}
