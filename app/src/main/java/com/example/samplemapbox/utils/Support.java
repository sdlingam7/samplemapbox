package com.example.samplemapbox.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.maps.model.LatLng;


/**
 * Created by ADMIN on 06-Apr-18.
 */

public class Support {

    public static AlertDialog alertDialog;
    public static AlertDialog.Builder alertDialogBuilder;
    public static SharedPreferences sharedpreferences;
    static Dialog pick_Dialog;
    private String TAG = Support.class.getSimpleName();

    @SuppressLint("HardwareIds")
    public static String GetDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isServiceRunning(Activity activity, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static LatLng formatLatLngFromString(String value) {
        if (value.equals("0.0,0.0")) {
            return null;
        }
        String[] S_Value = value.split(",");
        LatLng returnLatLng = null;
        if (S_Value.length == 2) {
            double Latitude = Double.parseDouble(S_Value[0]);
            double Longitude = Double.parseDouble(S_Value[1]);
            returnLatLng = new LatLng(Latitude, Longitude);
        }
        return returnLatLng;
    }


    public static void hideKeyboard(Context context) {
        try {
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            if ((((Activity) context).getCurrentFocus() != null) && (((Activity) context).getCurrentFocus().getWindowToken() != null)) {
                ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String ManageText(String Input) {
        Input = (Input == null) ? "" : Input;
        return Input.replace("'", "''");
    }


    public static void showProgress(View v) {
        v.setVisibility(View.VISIBLE);
    }

    public static void hideProgress(View v) {
        v.setVisibility(View.GONE);
    }


    public static boolean validateMobileNumber(String mobile_no) {
        return mobile_no.length() == 10;
    }

    public static boolean isMobile(String value) {
        boolean Result = true;
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); i++) {
            String Chars = value.charAt(i) + "";
            try {
                Integer.parseInt(Chars);
            } catch (Exception e) {
                Result = false;
            }
        }
        return Result;
    }

    public static boolean validatePassword(String password) {
        return password.length() > 5;
    }

    public static boolean validateMailId(String Mail_Id) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(Mail_Id).matches();
    }

    public static void SetPref(Context con, String Key, String Value) {
        sharedpreferences = con.getSharedPreferences("Pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(Key, Value);
        edit.commit();
    }

    public static String GetPref(Context con, String Key) {
        sharedpreferences = con.getSharedPreferences("Pref", Context.MODE_PRIVATE);
        String Value = "";
        if (sharedpreferences.contains(Key)) {
            Value = sharedpreferences.getString(Key, "");
        }
        return Value;
    }

    public static String GetPrefDefault(Context con, String Key, String Default) {
        sharedpreferences = con.getSharedPreferences("Pref", Context.MODE_PRIVATE);
        String Value = Default;
        if (sharedpreferences.contains(Key)) {
            Value = sharedpreferences.getString(Key, Default);
        }
        return Value;
    }

}
