package com.example.samplemapbox.utils;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface RetrofitObjectAPI {


//    https://api.mapbox.com/geocoding/v5/mapbox.places/Los%20Angeles.json?access_token=pk.eyJ1Ijoic2RsaW5nYW03IiwiYSI6ImNrMDIxaGR3eDBmZjYzY3FvZmlhNTB1MHkifQ.T-rBBI3eyeJaOP6uQe7xjw
    @GET("geocoding/v5/mapbox.places/{location}.json")
    Call<Object> getAddressByLatlng(@Path(value = "location", encoded = false) String location,
                                    @Query("access_token") String access_token);

}
