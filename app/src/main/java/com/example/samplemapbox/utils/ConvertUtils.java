package com.example.samplemapbox.utils;

public class ConvertUtils {

    public static String replaceDotZero(String value) {
        return value.replace(".0", "");
    }

    public static int ConvertToInteger(String value) {
        int result = 0;
        if (value != null && !value.trim().equals("")) {
            result = Integer.parseInt(value.trim());
        }
        return result;
    }

    public static double ConvertToDouble(String value) {
        double result = 0.0;
        if (value != null && !value.trim().equals("")) {
            result = Double.parseDouble(value.trim());
        }
        return result;
    }

    public static long ConvertToLong(String value) {
        long result = 0;
        try {
            if (value != null && !value.trim().equals("")) {
                result = Long.parseLong(value.trim());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static float ConvertToFloat(String value) {
        float result = 0f;
        try {
            if (value != null && !value.trim().equals("")) {
                result = Float.parseFloat(value.trim());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
